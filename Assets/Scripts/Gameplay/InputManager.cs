using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoM;

public class InputManager : MonoBehaviour
{
    public float m_MinCameraZoom = 5f;
    public float m_MaxCameraZoom = 40f;
    public float m_ScrollSpeed = 10f;

    private Camera m_Camera;

    void Start()
    {
        m_Camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        HandleCameraMove();

        if (Input.GetMouseButtonDown(0))
        {
            int location = CityManager.ConvertVector3ToLocation(
                m_Camera.ScreenToWorldPoint(Input.mousePosition)
            );
            Debug.Log(location);

            if (CityManager.Instance.CheckHasProductionCenter(location))
            {
                IProductionCenter productionCenter = CityManager.Instance.GetProductionCenter(
                    location
                );
                Debug.LogFormat("Found city {0} at location {1}", productionCenter.Name, location);
            }
            else
            {
                Debug.LogFormat("Empty space at {0}", location);
            }
        }
    }

    private void HandleCameraMove()
    {
        if (Input.GetKey("a"))
        {
            m_Camera.transform.Translate(new Vector3(-m_ScrollSpeed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey("d"))
        {
            m_Camera.transform.Translate(new Vector3(m_ScrollSpeed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey("w"))
        {
            m_Camera.transform.Translate(new Vector3(0, m_ScrollSpeed * Time.deltaTime, 0));
        }
        if (Input.GetKey("s"))
        {
            m_Camera.transform.Translate(new Vector3(0, -m_ScrollSpeed * Time.deltaTime, 0));
        }

        m_Camera.orthographicSize = m_Camera.orthographicSize - Input.mouseScrollDelta.y;
        if (m_Camera.orthographicSize < m_MinCameraZoom)
            m_Camera.orthographicSize = m_MinCameraZoom;
        else if (m_Camera.orthographicSize > m_MaxCameraZoom)
            m_Camera.orthographicSize = m_MaxCameraZoom;
    }
}
