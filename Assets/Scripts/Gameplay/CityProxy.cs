﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoM;

/*
    This class exposes the city object to the game.
    It is meant to act as an interface class for the underlying
    data stored in the city object, adding additional game
    specific functionality.
*/
public class CityProxy : MonoBehaviour
{
    public string m_CityName = "PLACEHOLDER NAME !";
    public int m_Population = 1000;
    public GameObject m_CityUIPrefab;
    public GameObject m_ItemInfoPrefab;

    private CityUIInfoManager m_UIInfo;
    private Canvas m_MainCanvas;
    private City m_City;

    List<Item> m_Products = new List<Item>();

    void Start()
    {
        m_City = new City();
        m_City.Population = m_Population;
        m_City.Name = m_CityName;

        m_Products.Add(new Item("Bricks", 5f, 8f));
        m_Products.Add(new Item("Planks", 8f, 12f));
        m_Products.Add(new Item("Furniture", 15f, 25f));
        m_Products.Add(new Item("Nails", 2f, 3f));
        m_Products.Add(new Item("Dye", 35f, 50f));

        m_MainCanvas = Object.FindObjectOfType<Canvas>();
        if (m_MainCanvas == null)
            Debug.LogWarning("No Canvas found :(");

        TimeManager.Instance.AddMonthlyCallback(
            new TimeManager.MonthlyFunction(m_City.MonthlyUpdateProductionCenter)
        );
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject CityUI = Instantiate(m_CityUIPrefab);

        CityUI.transform.SetParent(m_MainCanvas.transform, false);
        CityUI.SetActive(true);
        Debug.Log("Entering city of " + m_CityName);
        m_UIInfo = CityUI.GetComponent<CityUIInfoManager>();
        //m_UIInfo.gameObject.SetActive(true);
        m_UIInfo.SetCityInfo(m_City.Name, m_City.Population, m_Products);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        Debug.Log("Exiting city of " + m_CityName);
        m_UIInfo.gameObject.SetActive(false);
    }
}
