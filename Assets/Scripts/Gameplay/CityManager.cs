using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoM;

public class CityManager : SingletonMonoBehaviour<CityManager>
{
    Dictionary<int, IProductionCenter> CityDictionary;

    private static int Y_SHIFT = 10000;

    void Start()
    {
        CityDictionary = new Dictionary<int, IProductionCenter>();
        Debug.Log("Starting city manager");
    }

    public void RegisterNewProductionCenter(int location, IProductionCenter productionCenter)
    {
        CityDictionary.Add(location, productionCenter);
        Debug.LogFormat(
            "Registered new production center {0} at location {1}",
            productionCenter.Name,
            location
        );
    }

    public bool CheckHasProductionCenter(int location)
    {
        return CityDictionary.ContainsKey(location);
    }

    public IProductionCenter GetProductionCenter(int location)
    {
        return CityDictionary[location];
    }

    public static int ConvertVector3ToLocation(Vector3 coordinates)
    {
        return ((int)coordinates.x) + ((int)coordinates.y) * Y_SHIFT;
    }

    public static int ConvertVector2IntToLocation(Vector2Int coordinates)
    {
        return coordinates.x + coordinates.y * Y_SHIFT;
    }
}
