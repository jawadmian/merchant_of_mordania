﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float m_MinCameraZoom = 5f;
    public float m_MaxCameraZoom = 20f; 

    private Rigidbody2D m_Rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = gameObject.GetComponent<Rigidbody2D>();
        if(m_Rigidbody == null){
            Debug.LogError("Player does not have rigid body");
        } else {
            Debug.Log("Player rigid body found");
        }


    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A)) {
            MovePlayer(new Vector2(-1,0));
        }
        if(Input.GetKeyDown(KeyCode.D)) {
            MovePlayer(new Vector2(1,0));
        }
        if(Input.GetKeyDown(KeyCode.W)) {
            MovePlayer(new Vector2(0,1));
        }
        if(Input.GetKeyDown(KeyCode.S)) {
            MovePlayer(new Vector2(0,-1));
        }
        Camera.main.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y, -40), Quaternion.identity);

        //Debug.Log("--> delta" + Input.mouseScrollDelta.y);
        Camera.main.orthographicSize = Camera.main.orthographicSize - Input.mouseScrollDelta.y;
        if(Camera.main.orthographicSize < m_MinCameraZoom)
            Camera.main.orthographicSize = m_MinCameraZoom;
        else if (Camera.main.orthographicSize > m_MaxCameraZoom)
            Camera.main.orthographicSize = m_MaxCameraZoom;
    }



    private void MovePlayer(Vector2 direction) {
        m_Rigidbody.MovePosition(m_Rigidbody.position + direction);
    }

}
