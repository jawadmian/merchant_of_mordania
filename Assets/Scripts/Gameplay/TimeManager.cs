using UnityEngine;
using UnityEngine.UI;

/* 
    Class for tracking the time passage in the game and updating objects appropriately
*/

public class TimeManager : SingletonMonoBehaviour<TimeManager>
{
    public float m_DayDurationInSeconds = 1;
    public int m_DaysInMonth = 30;
    public int m_MonthsInYear = 12;
    
    [SerializeField] int m_CurrentDay = 1;
    [SerializeField] int m_CurrentMonth = 1;
    [SerializeField] int m_CurrentYear = 1;

    // UI fields, should move these somewher else to update
    [SerializeField] private Text m_DateTime;

    float m_TimeToNextDay;

    public delegate void DailyFunction();
    public delegate void MonthlyFunction();
    public delegate void YearlyFunction();

    private MonthlyFunction m_MonthlyFunction;
    void Start()
    {
        m_TimeToNextDay = m_DayDurationInSeconds;
        
    }
    void Update ()
    {
        m_TimeToNextDay -= Time.deltaTime;
        if (m_TimeToNextDay <= 0) {
            m_TimeToNextDay += m_DayDurationInSeconds;
            m_CurrentDay++;

            if (m_CurrentDay > m_DaysInMonth) {
                m_CurrentDay = 1;
                m_CurrentMonth++;
                m_MonthlyFunction();

                if (m_CurrentMonth > m_MonthsInYear) {
                    m_CurrentMonth = 1;
                    m_CurrentYear++;
                }
            }
            m_DateTime.text = GetDateString();
        }
        //Debug.Log("Update time :" + Time.deltaTime);
    }

    public string GetDateString()
    {
        return string.Format("Day: {0} / Month: {1} / Year: {2}", m_CurrentDay, m_CurrentMonth, m_CurrentYear);
    }

    public void AddMonthlyCallback(MonthlyFunction func) {
        m_MonthlyFunction += func;
    }
}
