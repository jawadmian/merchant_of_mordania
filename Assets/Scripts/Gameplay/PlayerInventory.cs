
using UnityEngine;
using MoM;

public class PlayerInventory : SingletonMonoBehaviour<PlayerInventory>
{
    private Inventory m_Inventory;

    void Start(){
        m_Inventory = new Inventory(1000);
    }

    public bool AddItem(Item item, int quantity) {
        return m_Inventory.AddItem(item, quantity);
    }
} 