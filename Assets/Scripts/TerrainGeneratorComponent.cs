﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TerrainGeneratorComponent : MonoBehaviour
{
    public Tilemap tilemap;

    public Tile tilebase;

    // Start is called before the first frame update
    void Start()
    {
        tilemap.BoxFill(Vector3Int.zero, tilebase, 0, 0, 1000, 1000);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
