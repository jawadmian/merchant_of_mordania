﻿namespace MoM
{
    /*
        the IProductionCenter interface indicates that this objects produces some type of item.
    */
    public interface IProductionCenter
    {
        string Name{
            get; 
            set;
        }

        void MonthlyUpdateProductionCenter();
    }
}
