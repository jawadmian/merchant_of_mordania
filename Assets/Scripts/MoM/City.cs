﻿using System.Collections.Generic;
using System;

namespace MoM
{
    /*
        Core data class for a city. The city implements the production center interface, 
        indicating that it can produce items.
    */
    public class City : IProductionCenter
    {
        List<Item> m_Products = new List<Item>();

        public int Population{get; set;}

        public float GrowthRate{get; set;}

        string m_CityName;
        public string Name{
            get => m_CityName; 
            set => m_CityName = value;
        }

        public City() {
            ItemManager im = ItemManager.GetInstance();
            GrowthRate = 1.1f;
        } 
        
        public void MonthlyUpdateProductionCenter() {
            Population = (int)(Population*GrowthRate);
        }
    }



}
