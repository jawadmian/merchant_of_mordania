using System.Collections.Generic;
using System;

// For debugging
using UnityEngine;

/* 
    A class representing an inventory.
*/
namespace MoM
{
    public class Inventory
    {
        int m_MaxCapacity;

        Dictionary<Item, int> m_Items = new Dictionary<Item, int>();

        public Inventory(int maxCapacity) {
            m_MaxCapacity = maxCapacity;
        }

        public bool AddItem(Item item, int quantity) {
            if(!m_Items.ContainsKey(item)){
                m_Items.Add(item,0);
            }

            m_Items[item] = m_Items[item] + quantity;
            Debug.Log("Adding item " + item.GetName() + " inventory contains " + m_Items[item]);
            return true; // DEFAULT VALUE
        }

        public bool RemoveItem(Item item, int quantity) {
            return true; // DEFAULT VALUE
        }
    }
}