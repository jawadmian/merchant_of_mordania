// Represents a single resouce item
namespace MoM
{
    public class Item
    {
        string m_ItemName = "Lumber";
        float m_MinPrice = 10.0f;
        float m_MaxPrice = 20.0f;

        public Item(string name, float min, float max) {
            m_ItemName = name;
            m_MinPrice = min;
            m_MaxPrice = max;
        }

        public string GetName() {
            return m_ItemName;
        }

        public float GetPrice() {
            return m_MinPrice;
        }
    }
}