﻿// A class for managing various in game items

using UnityEngine;
using System.Collections.Generic;

namespace MoM
{
    public class ItemManager
    {
        private static ItemManager instance;

        public static ItemManager GetInstance()
        {
            if (instance == null)
                instance = new ItemManager();
            return instance;
        }

        private ItemManager()
        {
            Item[] items = {
                new Item("Wood", 5f, 15f),
                new Item("Stone", 2f, 5f),
                new Item("Planks", 8f, 20f)
                };

            string jsontest = JsonUtility.ToJson(items);
            string text = System.IO.File.ReadAllText(@".\Assets\Data\Items.json");
            System.IO.File.WriteAllText(@".\Assets\Data\ItemsTest.json", jsontest);

        }


    }
}
