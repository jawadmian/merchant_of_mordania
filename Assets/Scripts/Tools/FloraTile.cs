﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEditor.Tilemaps;

namespace UnityEngine.Tilemaps
{
public class FloraTile : Tile 
{
    public Sprite[] m_Sprites;
    public Sprite m_Preview;
    // This refreshes itself and other FloraTiles that are orthogonally and diagonally adjacent
    public override void RefreshTile(Vector3Int location, ITilemap tilemap)
    {
        for (int yd = -1; yd <= 1; yd++)
            for (int xd = -1; xd <= 1; xd++)
            {
                Vector3Int position = new Vector3Int(location.x + xd, location.y + yd, location.z);
                if (HasFloraTile(tilemap, position))
                    tilemap.RefreshTile(position);
            }
    }
    // This determines which sprite is used based on the FloraTiles that are adjacent to it and rotates it to fit the other tiles.
    // As the rotation is determined by the FloraTile, the TileFlags.OverrideTransform is set for the tile.
    public override void GetTileData(Vector3Int location, ITilemap tilemap, ref TileData tileData)
    {
        int mask = HasFloraTile(tilemap, location + new Vector3Int(0, 1, 0)) ? 1 : 0; // Up
        mask += HasFloraTile(tilemap, location + new Vector3Int(1, 0, 0)) ? 2 : 0;  // Right
        mask += HasFloraTile(tilemap, location + new Vector3Int(0, -1, 0)) ? 4 : 0; // Down
        mask += HasFloraTile(tilemap, location + new Vector3Int(-1, 0, 0)) ? 8 : 0; // Left

        /*mask += HasFloraTile(tilemap, location + new Vector3Int(-1, 1, 0)) ? 16 : 0; // Top Left
        mask += HasFloraTile(tilemap, location + new Vector3Int(-1, -1, 0)) ? 32 : 0; // Bottom Left
        mask += HasFloraTile(tilemap, location + new Vector3Int(1, 1, 0)) ? 64 : 0; // Top Right
        mask += HasFloraTile(tilemap, location + new Vector3Int(1, -1, 0)) ? 128 : 0; // Bottom Right
        */
        int index = GetIndex((byte)mask);
        if (index >= 0 && index < m_Sprites.Length)
        {
            tileData.sprite = m_Sprites[index];
            tileData.color = Color.white;
            var m = tileData.transform;
            tileData.transform = m;
            tileData.flags = TileFlags.LockTransform;
            tileData.colliderType = ColliderType.None;
        }
        else
        {
        Debug.LogWarning("Not enough sprites in FloraTile instance");
}
    }
    // This determines if the Tile at the position is the same FloraTile.
    private bool HasFloraTile(ITilemap tilemap, Vector3Int position)
    {
        return tilemap.GetTile(position) == this;
    }
    // The following determines which sprite to use based on the number of adjacent FloraTiles
    private int GetIndex(byte mask)
    {
        // LDRT
        // 0000 = 00
        // 0001 = 01
        // 0010 = 02
        // 0011 = 03
        // 0100 = 04
        // 0101 = 05
        // 0110 = 06
        // 0111 = 07
        // 1000 = 08
        // 1001 = 09
        // 1010 = 10
        // 1011 = 11
        // 1100 = 12
        // 1101 = 13
        // 1110 = 14
        // 1111 = 15
        

        switch (mask)
        {
            case 0: return 0;
            case 1: return 0;
            case 2: return 0;
            case 3: return 0;
            case 4: return 0;
            case 5: return 0;
            case 6: return 0;
            case 7: return 0;
            case 8: return 0;
            case 9: return 0;
            case 10: return 0;
            case 11: return 0;
            case 12: return 0;
            case 13: return 0;
            case 14: return 0;
            case 15: return 0;
        }
        return -1;
    }

#if UNITY_EDITOR
// The following is a helper that adds a menu item to create a FloraTile Asset
    [MenuItem("Assets/Create/FloraTile")]
    public static void CreateFloraTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Flora Tile", "New Flora Tile", "Asset", "Save Flora Tile", "Assets");
        if (path == "")
            return;
    AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<FloraTile>(), path);
    }
#endif
}
}