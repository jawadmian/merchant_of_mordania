﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace UnityEditor.Tilemaps
{
#if UNITY_EDITOR
    [CustomGridBrush(true, false, false, "Checkered Brush")]
    public class CheckeredBrush : GridBrush
    {
        public Vector3Int lineStart = Vector3Int.zero;
        public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position)
        {
           if (((position.x + position.y) % 2) == 0)
                base.Paint(grid, brushTarget, position);

        }

        [MenuItem("Assets/Create/Checkered Brush")]
        public static void CreateBrush()
        {
            string path = EditorUtility.SaveFilePanelInProject("Save Checkered Brush", "New Checkered Brush", "Asset", "Save Checkered Brush", "Assets");
            if (path == "")
                return;
            AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<CheckeredBrush>(), path);
        }
    }

    [CustomEditor(typeof(CheckeredBrush))]
    public class CheckeredBrushEditor : GridBrushEditor
    {
        private CheckeredBrush CheckeredBrush { get { return target as CheckeredBrush; } }
        public override void OnPaintSceneGUI(GridLayout grid, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
        {
            base.OnPaintSceneGUI(grid, brushTarget, position, tool, executing);
        }
    }
#endif
}