﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using MoM;

public class CityUIItemManager : MonoBehaviour
{
    [SerializeField] private Text m_ItemName;
    [SerializeField] private Text m_ItemPrice;

    private Item m_Item;
    public void SetItemInfo(Item item){
        m_Item = item;
        m_ItemName.text = item.GetName();
        m_ItemPrice.text = item.GetPrice().ToString() + "$";
    }

    public void BuyItem() {
        Debug.Log("Bought item");
        PlayerInventory.Instance.AddItem(m_Item, 1);
    }

    public void SellItem() {
        Debug.Log("Sold item");
    }
}
