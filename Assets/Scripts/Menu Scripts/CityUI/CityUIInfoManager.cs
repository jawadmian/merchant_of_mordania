using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoM;


/*
    Class for setting the informaition on the city UI, this function is called by the city manager
*/
public class CityUIInfoManager : MonoBehaviour
{
    [SerializeField] private Text m_CityName;
    [SerializeField] private Text m_Population;
    [SerializeField] private Transform m_ItemGrid;
    [SerializeField] private GameObject m_ItemPrefab;

    public void SetCityInfo(string name, int population, List<Item> itemList) {
        m_CityName.text = name;
        m_Population.text = "Population: " + population.ToString();
        foreach (Item item in itemList) {
            Debug.Log("item in city " + item.GetName());
            GameObject itemUIEntry = Instantiate(m_ItemPrefab);
            CityUIItemManager itemManager = itemUIEntry.GetComponent<CityUIItemManager>();
            itemManager.SetItemInfo(item);
            itemUIEntry.transform.SetParent(m_ItemGrid, false);
        }
    }
}
