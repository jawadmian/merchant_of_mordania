using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using MoM;

public class SimpleWorldGenerator : MonoBehaviour
{
    [SerializeField]
    Vector2Int m_WorldSize = new Vector2Int(10, 10);

    [SerializeField]
    float m_Scale = 0.3f;

    [SerializeField]
    Tilemap m_WorldTileMap;

    [SerializeField]
    Tilemap m_ObjectsTileMap;

    [SerializeField]
    TileBase m_GrassTile;

    [SerializeField]
    TileBase m_WaterTile;

    [SerializeField]
    TileBase m_SandTile;

    [SerializeField]
    TileBase[] m_CityTiles;

    [SerializeField]
    int m_Seed = 1;

    byte[,] m_WorldMap;

    // Start is called before the first frame update
    void Start()
    {
        CreateMap();
    }

    public void CreateRandom()
    {
        m_Seed = (int)Random.Range(0, 1000000f);
        CreateMap();
        m_ObjectsTileMap.ClearAllTiles();
    }

    public void CreateMap()
    {
        Debug.Log("Creating tiles");
        float[,] noiseMap = GenerateNoiseMap(
            m_WorldSize.x,
            m_WorldSize.y,
            m_Seed,
            m_Scale,
            4,
            0.2f,
            4,
            new Vector2(1f, 1f)
        );

        m_WorldMap = CreateWorldMapFromNoiseMap(noiseMap);

        for (int i = 0; i < m_WorldSize.x; i++)
        {
            for (int j = 0; j < m_WorldSize.y; j++)
            {
                m_WorldTileMap.SetTile(new Vector3Int(i, j, 0), TileSelector(i, j));
            }
        }
    }

    TileBase TileSelector(int x, int y)
    {
        switch (m_WorldMap[x, y])
        {
            case 0:
                return m_WaterTile;
            case 1:
                return m_SandTile;
            default:
                return m_GrassTile;
        }
    }

    public static float[,] GenerateNoiseMap(
        int mapWidth,
        int mapHeight,
        int seed,
        float scale,
        int octaves,
        float persistence,
        float lacunarity,
        Vector2 offset
    )
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];
        int randRange = 10000;
        for (int i = 0; i < octaves; i++)
        {
            octaveOffsets[i] = new Vector2(
                prng.Next(-randRange, randRange) + offset.x,
                prng.Next(-randRange, randRange) + offset.y
            );
        }

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;
                for (int i = 0; i < octaves; ++i)
                {
                    float sampleX = x / scale * frequency + octaveOffsets[i].x;
                    float sampleY = y / scale * frequency + octaveOffsets[i].y;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;
                    amplitude *= persistence;
                    frequency *= lacunarity;
                }

                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }

                if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }
                noiseMap[x, y] = noiseHeight;
            }
        }

        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
            }
        }

        return noiseMap;
    }

    public byte[,] CreateWorldMapFromNoiseMap(float[,] noiseMap)
    {
        byte[,] worldMap = new byte[m_WorldSize.x, m_WorldSize.y];
        for (int x = 0; x < m_WorldSize.x; x++)
        {
            for (int y = 0; y < m_WorldSize.y; y++)
            {
                if (noiseMap[x, y] < 0.4)
                    worldMap[x, y] = 0;
                else if (noiseMap[x, y] < 0.5)
                    worldMap[x, y] = 1;
                else
                    worldMap[x, y] = 2;
            }
        }
        return worldMap;
    }

    public void PlaceCity()
    {
        bool locationFound = false;
        Vector2Int worldPos;

        while (!locationFound)
        {
            worldPos = new Vector2Int(
                Random.Range(0, m_WorldSize.x),
                Random.Range(0, m_WorldSize.y)
            );

            if (m_WorldMap[worldPos.x, worldPos.y] != 0)
            {
                locationFound = true;
                Debug.Log("Found a valid city placement location at " + worldPos);
                m_ObjectsTileMap.SetTile(new Vector3Int(worldPos.x, worldPos.y, 0), m_CityTiles[0]);
                IProductionCenter pc = new City();
                pc.Name = "Test City";
                CityManager.Instance.RegisterNewProductionCenter(
                    CityManager.ConvertVector2IntToLocation(worldPos),
                    pc
                );
            }
        }
    }
}
